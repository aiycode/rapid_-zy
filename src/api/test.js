import request from '@/utils/request'

export function testDCM() {
  return request({
    baseURL: 'http://192.168.226.130/isvapi/service',
    url: '/v1/patientmgmt/rawpixel',
    method: 'get',
    params: {
      sitename: 'huamedtechtest2',
      seriesinstanceuid: '1.2.392.200036.9116.2.5.1.3268.2046851640.1504160857.514841',
      taskid: 35,
      slab: 0,
      band: 0,
      timepoint: 0,
      offset: 7340032,
      framesize: 524288,
      frametype: ''
    }
  })
}
