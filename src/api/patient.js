import request from '@/utils/request'

export function patientViewsList(query) {
  return request({
    url: '/vue-element-admin/patient/viewsList',
    method: 'get',
    params: query
  })
}
